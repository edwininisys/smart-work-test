import React, { useCallback, useEffect, useState } from "react";

import { getRestaurants, createRestaurants } from "../api/restaurants";
import { Container } from "../components/Container";
import { RestaurantList } from "../components/RestaurantList";
import { RestaurantCreate } from "../components/RestaurantCreate";
import {
  RestaurantFilters,
  RestaurantFiltersState,
} from "../components/RestaurantFilters";
import { Restaurant } from "../interfaces/restaurant";
import { NavbarLink } from "../components/NavbarLink";
import * as AuthService from "../auth/authService";

export function Restaurants() {
  const [tags, setTags] = useState<string[]>([]);
  const [checkedTags, setcheckedTags] = useState<string[]>([]);
  const [restaurants, setRestaurants] = useState<Restaurant[]>([]);
  const [restaurantsbak, setRestaurantsbak] = useState<any[]>([]);
  const [isCreatePage, setisCreatePage] = useState<boolean>(false);
  const [isAuthenticated, setAuthenticated] = useState(false);
  async function fetchRestaurants() {
    const data = await getRestaurants();
    setRestaurants(data);
    console.log(data);
    setRestaurantsbak(data);
    let tagValues = data.flatMap((x) => x.tags);
    if (tagValues !== null) {
      let tagFiltes = tagValues.filter(function (item, pos, self) {
        return self.indexOf(item) == pos;
      });
      setTags(tagFiltes);
    }

  }
  useEffect(() => {
   

    //async function fetchRestaurantbaks() {
    //  const data = await getRestaurants();      
    //  console.log(data);
    //  setRestaurantsbak(data);
      
    async function checkAuthenticated() {
      const authenticated = await AuthService.authService.isAuthenticated();
      setAuthenticated(authenticated);
    }
    //}
    checkAuthenticated();
    fetchRestaurants();
  }, []);
  async function createRestaurant(restrarent:Restaurant) {
    const data = await createRestaurants(restrarent);
    return data;

  }
  const handleFiltersChange = useCallback((value: RestaurantFiltersState) => {

    let nextRestaurantsold = restaurantsbak;
    setRestaurantsbak((newp: any) => {
      nextRestaurantsold = newp;
      return newp;
    });       
    setRestaurants((nextRestaurants: any) => {
      // if (restaurantsbak.length == 0) {
      //   setRestaurantsbak(nextRestaurants);
      // }
      let chcTags:string[] = [];
        if(value.searchKey!==null){
          if (value.searchKey.length > 0) {
            let res = nextRestaurantsold.filter((r: any) => r.name.toLowerCase().includes(value.searchKey));
            nextRestaurants = res;
          }
          else {
            nextRestaurants = nextRestaurantsold;
          }
        }
        else {
          nextRestaurants = nextRestaurantsold;
        }
      if (value.tags.length) {
        value.tags.forEach((tag) => {
          nextRestaurants = nextRestaurants.filter((r:any) => r.tags.includes(tag));
        });
      }

      if (value.isFamilyFriendly) {
        nextRestaurants = nextRestaurants.filter((r: any) => r.familyFriendly);
        let tagValues = nextRestaurants.flatMap((x:any) => x.tags);
        if (tagValues !== null) {
          let tagFiltes = tagValues.filter(function (item: any, pos: any, self: string | any[]) {
            return self.indexOf(item) == pos;
          });
          chcTags=tagFiltes;
        }
      }

      if (value.isVeganFriendly) {
        nextRestaurants = nextRestaurants.filter((r: any) => r.veganFriendly);
        let tagValues = nextRestaurants.flatMap((x: any) => x.tags);
        if (tagValues !== null) {
          let tagFiltes = tagValues.filter(function (item: any, pos: any, self: string | any[]) {
            return self.indexOf(item) == pos;
          });
          chcTags =chcTags.concat(tagFiltes);
        }
      }
      setcheckedTags(chcTags);

      return nextRestaurants;
    });
  },[]);
  const onSubmit = useCallback((value: Restaurant) => {
    if (value == null) {
      setisCreatePage(false);
    }
    else {
      createRestaurant(value).then((res) => {
        if (res.id > 0) {
          fetchRestaurants()
          setisCreatePage(false);
        }
      }).catch((res) => {
        console.log(res);
      });
    }
  }, []);
  const onBackPress = useCallback((value: boolean) => {
    if (value == true) {
      setisCreatePage(false);
    }
    
  }, []);
  return (
    <div className="row">
      {isAuthenticated==true && !isCreatePage && (
      <div>
      <div className="row">
        <div className="col-lg-10">
        </div>
        <div className="col-lg-2">
              <button className="btn btn-success" value="Create" onClick={(e) => { setisCreatePage(true)}}>Create</button>
        </div>
      </div>
    
        </div>
      )}
      {!isCreatePage && (
        <Container>
          <RestaurantFilters tags={tags} checkedTags={checkedTags} onChange={handleFiltersChange} />
          <RestaurantList restaurants={restaurants} />
        </Container>
        )}
      {isCreatePage && (<RestaurantCreate onBack={onBackPress} SubmitCallback={onSubmit}/>)}
    </div>
    //<div className="row">
    //<Container>
    //  <RestaurantFilters tags={tags} checkedTags={checkedTags} onChange={handleFiltersChange} />
    //  <RestaurantList restaurants={restaurants} />
    //  </Container>
    //</div>
  );
}
