import { Restaurant } from "../interfaces/restaurant";

export interface RestaurantItemProps {
  restaurant: Restaurant;
}

export function RestaurantItem(props: RestaurantItemProps) {
  const { restaurant } = props;
  return (
    <div className="row item-row">
      <div >
        <img
          className= "photo-img"         
          src={restaurant.photoUri}
        />
      </div>

      <h3>{restaurant.name}</h3>      
      <div><p>{restaurant.description}</p></div>
      <div>{restaurant.address}</div>
      <div>{restaurant.phoneNumber}</div>
    </div>
  );
}
