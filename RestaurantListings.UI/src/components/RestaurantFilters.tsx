import React, { useReducer, Reducer, useEffect } from "react";
import styled from "@emotion/styled";

import { ToggleFilter } from "./ToggleFilter";

export interface RestaurantFiltersState {
  tags: string[];
  isFamilyFriendly: boolean;
  isVeganFriendly: boolean;
  searchKey:string |null;
}

type RestaurantFiltersAction =
  | { type: "toggleTag"; payload: { tag: string } }
  | { type: "toggleFamilyFriendly" }
  | { type: "toggleSearch";payload: { key: string } }
  | { type: "toggleVeganFriendly" };

const restaurantFiltersReducer: Reducer<
  RestaurantFiltersState,
  RestaurantFiltersAction
> = (state, action) => {
  switch (action.type) {
    case "toggleTag": {
      const tagIndex = state.tags.indexOf(action.payload.tag);
      if (tagIndex !== -1) {
        return { ...state, tags: state.tags.splice(tagIndex, 1) };
      } else {
        return { ...state, tags: [...state.tags, action.payload.tag] };
      }
    }
    case "toggleSearch":{
      return { ...state, searchKey: action.payload.key };
    }

    case "toggleFamilyFriendly": {
      return { ...state, isFamilyFriendly: !state.isFamilyFriendly };
    }

    case "toggleVeganFriendly": {
      // state.isVeganFriendly = !state.isVeganFriendly;
      // return state;
      return { ...state, isVeganFriendly: !state.isVeganFriendly };
    }

    default:
      throw Error();
  }
};

export interface RestaurantFiltersProps {
  tags: string[];
  checkedTags: string[];
  onChange?: (value: RestaurantFiltersState) => unknown;
}

const FilterGroup = styled.div({
  marginBottom: "1rem",
});

const FilterTitle = styled.h4({
  marginBottom: "1rem",
  fontSize: "1.1rem",
  fontWeight: 500,
  color: "#2c2c2c",
});

export function RestaurantFilters(props: RestaurantFiltersProps) {
  const { tags = [], checkedTags=[], onChange } = props;
  const [state, dispatch] = useReducer(restaurantFiltersReducer, {
    tags: [],
    isFamilyFriendly: false,
    isVeganFriendly: false,
    searchKey:""
  });

  useEffect(() => {
    onChange?.(state);
  }, [state, onChange]);
  function getIsChecked(tag: any) {
    if (state.tags.includes(tag)) {
      return true;
    }
    if (props.checkedTags.includes(tag)) {
      return true;
    }
    return false;
  }
  
  return (
    <div className="filter-container" >
      <div >
        <label htmlFor="header-search">
          <span className="visually-hidden">Search blog posts</span>
        </label>
        <input
          type="text"
          id="header-search"
          placeholder="Search Restaurants"
          name="s"
          value={state.searchKey ? state.searchKey : ""}
          onChange={(e) => {
            dispatch({ type: "toggleSearch", payload: { key: e.target.value } })
          }}
        />
      </div>
      <FilterGroup>
        <FilterTitle>Tags</FilterTitle>

        {tags.sort().map((tag, index) => (
          <ToggleFilter key={index}
            label={tag}
            isChecked={getIsChecked(tag)}
            onChange={() => dispatch({ type: "toggleTag", payload: { tag } })}
          />
        ))}
      </FilterGroup>

      <FilterGroup>
        <FilterTitle>Other</FilterTitle>

        <ToggleFilter
          label="Family friendly"
          isChecked={state.isFamilyFriendly}
          onChange={() => dispatch({ type: "toggleFamilyFriendly" })}
        />

        <ToggleFilter
          label="Vegan"
          isChecked={state.isVeganFriendly}
          onChange={() => dispatch({ type: "toggleVeganFriendly" })}
        />
      </FilterGroup>
    </div>
  );
}
