import styled from "@emotion/styled";

export const Container = styled.div({
  margin: "0 auto",
  width: "100%",
  padding: "0 1rem",
  display: "flex",
  flexDirection: "row",
  gap: "2rem",
});


export const CenterContainer = styled.div({
  margin: "6% 10% 10% 28%",
  width: "100%",
  padding: "0 1rem",
  flexDirection: "row",
  gap: "2rem",
});
