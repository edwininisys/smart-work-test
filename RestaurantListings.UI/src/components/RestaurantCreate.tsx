import React, { useEffect, useState } from "react";

import { Restaurant } from "../interfaces/restaurant";
import { CenterContainer, Container } from "./Container";

export interface RestaurantProps {
  SubmitCallback: (value: Restaurant) => unknown;
  onBack: (value: boolean) => unknown;
}

export function RestaurantCreate(props: RestaurantProps) {
  let res: Restaurant = { photoUri : "/images/brunswick.jpg",address:"",description:"", id:0,familyFriendly:false, name:"", phoneNumber:"",rating:0, tags:["Asia"],veganFriendly:false};
  const { SubmitCallback, onBack} = props;
  const [restaurant, setRestaurant] = useState<Restaurant>(res);  
  const handleChange= (e: any)=>{
    const { name, value, checked } = e.target;
    switch (name) {
      case 'name':
        setRestaurant({ ...restaurant, name: value });
                  
        break;
      case 'description':
                        
        setRestaurant({ ...restaurant, description: value });
        break;
      case 'address':
        setRestaurant({ ...restaurant, address: value });
                  
        break;
      case 'phoneNumber':        
        setRestaurant({ ...restaurant, phoneNumber: value });
                  
        break;
      case 'veganFriendly':        
        setRestaurant({ ...restaurant, veganFriendly: checked });
                  
        break;
      case 'familyFriendly':        
        setRestaurant({ ...restaurant, familyFriendly: checked});
                  
        break;
      default:
        break;
    }
  }
  const onSubmitHandler = (e: { target: any, preventDefault: () => void; })=>{
    e.preventDefault();
    if (e.target.id == "back") {
      props.onBack(true);
    }
    else {
      props.SubmitCallback(restaurant);
    }
    
  }
  
  return (
    <CenterContainer>
      
      <br />
      <br />
      <form name="contactform" onSubmit={(e) => { onSubmitHandler(e) }}>
        <div className="row">
        <div className="col-lg-6 col-md-6">
      <fieldset>
          <input name="name" type="text"  placeholder="Name" required value={restaurant.name} onChange={(e) => { handleChange(e)} } />
            <br />
            <br />
            <input name="address" type="text" placeholder="Address" required value={restaurant.address} onChange={(e) => { handleChange(e) }}/>
            <br />
            <br />
            <input name="phoneNumber" type="tel" placeholder="Phone"
              pattern="^\+[0-9]{2}-[0-9]{3}-[0-9]{3}-[0-9]{4}"
              title="Phone number should be like +44-011-355-5955"
              required value={restaurant.phoneNumber} onChange={(e) => { handleChange(e) }} />
            <br />
            <br />
          </fieldset>
          <fieldset>
            <textarea name="description" className="comments" placeholder="Details" required value={restaurant.description} onChange={(e) => { handleChange(e) }} />
            <br />
            <br />
          </fieldset>

          <fieldset>
            <div>
              <label>
                <input
                  type="checkbox"
                  name="veganFriendly"
                  checked={restaurant.veganFriendly} onChange={(e) => { handleChange(e) }}
                />
                Vegan Friendly
              </label>
            </div>
            <br />
          </fieldset>
          <fieldset>
            <div>
              <label>
                <input
                  type="checkbox"
                  name="familyFriendly"
                  checked={restaurant.familyFriendly} onChange={(e) => { handleChange(e) }}
                />
                Family Friendly
              </label>
            </div>
            <br />
          </fieldset>
    </div>
    </div>
    <div className="row">
          <div className="col-lg-3 col-md-6">
            <fieldset>
              <button className="btn btn-success " id="submit" value="Submit">
                Create
        </button>
              <label className="btn btn-danger" onClick={() => { props.onBack(true); }}>Cancel</label>
            </fieldset>
          </div>
          <div className="col-lg-3 col-md-6">
           
          </div>
    </div>
      </form>
    </CenterContainer>
  );
}


