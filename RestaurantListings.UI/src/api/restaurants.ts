import { Restaurant } from "../interfaces/restaurant";

export async function getRestaurants(): Promise<Restaurant[]> {
  const res = await fetch("/api/restaurants");
  return await res.json();
}

export async function createRestaurants(Restaurant: Restaurant): Promise<Restaurant> {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(Restaurant)
  };
  const res = await fetch("/api/restaurants", requestOptions);
  return await res.json();
}
